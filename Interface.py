from tkinter import *
import tkinter as tk

raiz=Tk()
frame1=Frame(raiz)
raiz.title("Calculadora")
raiz.resizable(False,False)
raiz.iconbitmap("calc.ico")# Icono
raiz.geometry("230x300")# Ancho y Alto
raiz.config(bg="#c5e1a5")

# Indica la posicion 
indice = 0
# Contiene el string de la pantalla 1
Lista = ""
# Indica posicion
indicador = 0
# Contiene el string de la pantalla 2
pan2 = ""
# Contiene la ultima letra o - + * / 
ultima = ""
# Contiene el string de la pantalla 1 o pantalla 2
talla = ""
# Indica posicion del punto, lo coloque aparte para evitar errores
indicadorP = 0

# frame 2
frame2=Frame()
frame2.place(x=8,y=10)
frame2.config(bg="#f8ffd7")#f8ffd7
frame2.config(width="310", height="80")

# frame 3
frame3=Frame()
frame3.place(x=3,y=80)
frame3.config(bg="#c5e1a5")
frame3.config(width="224", height="215")

# PANTALLA 1
numeroPantalla=StringVar()
pantalla=Entry(frame2, width=16, bg="#f8ffd7", fg="#94af76",font=("Comic Sans MS",15),
                                                                    borderwidth=0,
                                                                    textvariable=numeroPantalla,
                                                                    justify="right")
pantalla.place(x=5,y=8)

# PANTALLA 2
numeroPantalla2=StringVar()
pantalla_2=Entry(frame2, width=29, bg="#f8ffd7", fg="#94af76",font=("Comic Sans MS",8),
                                                                    borderwidth=0,
                                                                    textvariable=numeroPantalla2,
                                                                    justify="right")
pantalla_2.place(x=5,y=40)
class pasar_enima(tk.Button):
    def __init__(self, master, **kw):
        tk.Button.__init__(self,master=master,**kw)
        self.defaultBackground = self["background"]
        self.bind("<Enter>", self.on_enter)
        self.bind("<Leave>", self.on_leave)
    def on_enter(self, e):
        self['background'] = self['activebackground']
    def on_leave(self, e):
        self['background'] = self.defaultBackground

# fila 1 de botones
botonBorrar=pasar_enima(frame3, text="AC", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,command=lambda:borrar(),activebackground="#73885c")
botonBorrar.place(x=4,y=5)
boton8=pasar_enima(frame3, text="(", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,command=lambda:numeroPulsado("("),activebackground="#73885c")
boton8.place(x=60,y=5)
boton9=pasar_enima(frame3, text=")", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,command=lambda:numeroPulsado(")"),activebackground="#73885c")
boton9.place(x=116,y=5)
botondiv=pasar_enima(frame3, text="/", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,activebackground="#73885c")
botondiv.place(x=172,y=5)

# fila 2 de botones
boton7=pasar_enima(frame3, text="7", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,command=lambda:numeroPulsado("7"),activebackground="#73885c")
boton7.place(x=4,y=47)
boton8=pasar_enima(frame3, text="8", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,command=lambda:numeroPulsado("8"),activebackground="#73885c")
boton8.place(x=60,y=47)
boton9=pasar_enima(frame3, text="9", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,command=lambda:numeroPulsado("9"),activebackground="#73885c")
boton9.place(x=116,y=47)
botonmul=pasar_enima(frame3, text="*", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,command=lambda:multiplicar(),activebackground="#73885c")
botonmul.place(x=172,y=47)

# fila 3 de botones
boton4=pasar_enima(frame3, text="4", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,command=lambda:numeroPulsado("4"),activebackground="#73885c")
boton4.place(x=4,y=89)
boton5=pasar_enima(frame3, text="5", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,command=lambda:numeroPulsado("5"),activebackground="#73885c")
boton5.place(x=60,y=89)
boton6=pasar_enima(frame3, text="6", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,command=lambda:numeroPulsado("6"),activebackground="#73885c")
boton6.place(x=116,y=89)
botonmenos=pasar_enima(frame3, text="-", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,command=lambda:resta(),activebackground="#73885c")
botonmenos.place(x=172,y=89)

# fila 4 de botones
boton1=pasar_enima(frame3, text="1", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,command=lambda:numeroPulsado("1"),activebackground="#73885c")
boton1.place(x=4,y=131)
boton2=pasar_enima(frame3, text="2", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,command=lambda:numeroPulsado("2"),activebackground="#73885c")
boton2.place(x=60,y=131)
boton3=pasar_enima(frame3, text="3", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0, command=lambda:numeroPulsado("3"),activebackground="#73885c")
boton3.place(x=116,y=131)
botonmas=pasar_enima(frame3, text="+", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0, command=lambda:suma(),activebackground="#73885c")
botonmas.place(x=172,y=131)

# fila 5 de botones
boton0=pasar_enima(frame3, text="0", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0, command=lambda:numeroPulsado("0"),activebackground="#73885c")
boton0.place(x=4,y=173)
botoncoma=pasar_enima(frame3, text=".", width=6, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0,activebackground="#73885c")
botoncoma.place(x=60,y=173)
botonigual=pasar_enima(frame3, text="=", width=14, height=2, font=("Comic Sans MS",8),
                                                        bg="#94af76", fg="white",bd=0, command=lambda:operacion(),activebackground="#73885c")
botonigual.place(x=116,y=173)

# FUNCIONES TECLADO

# Funcion de numero pulsado
def numeroPulsado(valor):
    global indice
    pantalla.insert(indice, valor)
    indice += 1

# Funciones -
def resta():
    global ultima
    global talla
    global indicador
    global Lista
    global pan2
    ultima = ""
    #Condicion para que no se pueda precionar varias veces
    if pantalla_2.get() == "-" or pantalla.get() == "" or "+" == ultima or "-" == ultima or "/" == ultima or "*" == ultima:
        print(":3")
    else:
        talla = ""
        Lista = pantalla.get() + "-"
        pantalla.delete(0, END)
        pantalla_2.insert(indicador, Lista)
        indicador =  len(str(pantalla_2.get()))
        talla = pantalla_2.get()
        ultima = talla[indicador-1]
        pan2 = pantalla_2.get()

# Funciones +
def suma():
    global ultima
    global talla
    global indicador
    global Lista
    global pan2
    ultima = ""
    #Condicion para que no se pueda precionar varias veces
    if pantalla_2.get() == "+" or pantalla.get() == "" or "+" == ultima or "-" == ultima or "/" == ultima or "*" == ultima:
        print(":v")
    else:
        talla = ""
        Lista = pantalla.get() + "+"
        pantalla.delete(0, END)
        pantalla_2.insert(indicador, Lista)
        indicador =  len(str(pantalla_2.get()))
        talla = pantalla_2.get()
        ultima = talla[indicador-1]
        pan2 = pantalla_2.get()

# Funciones *
def multiplicar():
    global ultima
    global talla
    global indicador
    global Lista
    global pan2
    ultima = ""
    #Condicion para que no se pueda precionar varias veces 
    if pantalla_2.get() == "*" or pantalla.get() == "" or "+" == ultima or "-" == ultima or "/" == ultima or "*" == ultima:
        print(":o")
    else:
        talla = ""
        Lista = pantalla.get() + "*"
        pantalla.delete(0, END)
        pantalla_2.insert(indicador, Lista)
        indicador =  len(str(pantalla_2.get()))
        talla = pantalla_2.get()
        ultima = talla[indicador-1]
        pan2 = pantalla_2.get()

# Funcion AC
def borrar():
    global indice
    pantalla.delete(0, END)
    pantalla_2.delete(0, END)
    indice = 0

# Funcion =
def operacion():
    global indice
    global pan2
    global indicador
    #Condicion para que no se pueda precionar varias veces 
    if pantalla_2.get() == "" or pantalla.get() == "":
        print(":c")
    else:
        pantalla_2.delete(0, END)
        ecuacion = pan2
        ecuacion2 = pantalla.get()
        pantalla_2.delete(0, END)
        resultado = eval(ecuacion + ecuacion2)
        pantalla.delete(0, END)
        pantalla.insert(0, resultado)
        pantalla_2.insert(0, ecuacion + ecuacion2)
        indice = len(str(resultado))
        indicador =  len(str(pantalla_2.get()))
        pan2 = ""
# para que se visible la ventana
raiz.mainloop()
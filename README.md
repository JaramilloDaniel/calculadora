# PROYECTO MISIONTIC 2022

## INTEGRANTES
- Diego Paz
- Juan Cardona
- Mario Prado
- Daniel Jaramillo
- Lina Cardona 


## Descripción

Proyecto creado para el desarrollo de calculadora con las operaciones básicas: Suma, resta, multiplicación y división.

Para clonar el proyecto:
```
git clone https://linamar126@bitbucket.org/JaramilloDaniel/calculadora.git
```
